package com.example.photounsplash.Models

import com.google.gson.annotations.SerializedName

class ProfileImage {

    @SerializedName("small")
    var small: String = ""
        get() = field
        set(value) {
            field = value
        }
    @SerializedName("medium")
    var medium: String = ""
        get() = field
        set(value) {
            field = value
        }

}