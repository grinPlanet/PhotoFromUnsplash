package com.example.photounsplash.Models

class Location {

    /**
     * "title": "Kitsuné Café, Montreal, Canada"
     * "name": "Kitsuné Café"
     * "city": "Montreal",
     * "country": "Canada",
     * "position": {
     * "latitude": 45.4732984,
     * "longitude": -73.6384879
     * }
     */

    var title: String? = null
    var name: String? = null
    var city: String? = null
    var country: String? = null

    var position: Position? = null

    class Position {
        var latitude: Double = 0.toDouble()
        var longitude: Double = 0.toDouble()
    }

}