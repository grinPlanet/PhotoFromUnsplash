package com.example.photounsplash.Models

import com.google.gson.annotations.SerializedName

class Collection {
    @SerializedName("id")
    var id: Int = 0

    @SerializedName("title")
    var title: String = ""

    @SerializedName("description")
    var description: String = ""

    @SerializedName("total_photos")
    var totalPhotos: Int = 0

    @SerializedName("cover_photo")
    var coverPhoto: Photo = Photo()

    @SerializedName("user")
    var user: User = User()

    var published_at: String? = null
    var updated_at: String? = null
    var curated: Boolean = false

    var privateX: Boolean = false
    var share_key: String? = null

    var links: CollectionLinks? = null

    var tags: List<Tag>? = null

}