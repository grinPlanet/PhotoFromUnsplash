package com.example.photounsplash.Models

import com.google.gson.annotations.SerializedName

class PhotoUrl {

    /**
     * "raw": "https://images.unsplash.com/photo-1417325384643-aac51acc9e5d",
     * "full": "https://images.unsplash.com/photo-1417325384643-aac51acc9e5d?q=75&fm=jpg",
     * "regular": "https://images.unsplash.com/photo-1417325384643-aac51acc9e5d?q=75&fm=jpg&w=1080&fit=max",
     * "small": "https://images.unsplash.com/photo-1417325384643-aac51acc9e5d?q=75&fm=jpg&w=400&fit=max",
     * "thumb": "https://images.unsplash.com/photo-1417325384643-aac51acc9e5d?q=75&fm=jpg&w=200&fit=max"
     */

    @SerializedName("full")
    var full: String = ""
        get() = field
        set(value) {
            field = value
        }
    @SerializedName("regular")
    var regular: String = ""
        get() = field
        set(value) {
            field = value
        }

    var raw: String? = null
    var small: String? = null
    var thumb: String? = null


}