package com.example.photounsplash.Models

import com.google.gson.annotations.SerializedName

class User {

    @SerializedName("id")
    var id: String = ""
        get() = field
        set(value) {
            field = value
        }
    @SerializedName("username")
    var username: String = ""
        get() = field
        set(value) {
            field = value
        }
    @SerializedName("profile_image")
    var profileImage: ProfileImage = ProfileImage()
        get() = field
        set(value) {
            field = value
        }

    var name: String ?= null

}