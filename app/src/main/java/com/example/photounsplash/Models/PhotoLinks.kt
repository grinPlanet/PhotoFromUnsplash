package com.example.photounsplash.Models

class PhotoLinks {

    /**
     * "self": "https://api.unsplash.com/photos/LBI7cgq3pbM",
     * "html": "https://unsplash.com/photos/LBI7cgq3pbM",
     * "download": "https://unsplash.com/photos/LBI7cgq3pbM/download",
     * "download_location": "https://api.unsplash.com/photos/LBI7cgq3pbM/download"
     */

    var self: String? = null
    var html: String? = null
    var download: String? = null
    var download_location: String? = null

}