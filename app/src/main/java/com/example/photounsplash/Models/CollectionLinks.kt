package com.example.photounsplash.Models

class CollectionLinks {

    /**
     * "self": "https://api.unsplash.com/collections/296",
     * "html": "https://unsplash.com/collections/296",
     * "photos": "https://api.unsplash.com/collections/296/photos",
     * "related": "https://api.unsplash.com/collections/296/related"
     */

    var self: String? = null
    var html: String? = null
    var photos: String? = null
    var related: String? = null

}