package com.example.photounsplash.Models

class Tag {

    /**
     * title : frozen
     * url : https://images.unsplash.com/photo-1420466721261-818d807296a1
     */

    var title: String? = null
    var url: String? = null
}