package com.example.photounsplash.Models

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.example.photounsplash.R
import com.example.photounsplash.Utils.SquareImage
import com.google.gson.annotations.SerializedName
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem

class Photo : AbstractItem<Photo.ViewHolder>() {

    /*
      "id": "pFqrYbhIAXs",
      "created_at": "2017-05-30T17:30:44-04:00",
      "updated_at": "2017-10-31T17:12:31-04:00",
      "width": 3456,
      "height": 2304,
      "color": "#A49E9B",
      "likes": 355,
      "user": {
        "id": "oB1kn_oBee",
        "updated_at": "2018-04-10T13:27:21-04:00",
        "username": "lukeskywalker",
        "name": "Luke Skywalker",
        "first_name": "Luke",
        "last_name": "Skywalker",
        "twitter_username": "HamillHimself",
        "portfolio_url": "https://unsplash.com",
        "bio": "I'm a hermit on a water planet. I don't need people. Please don't leave me. Gifted in the ways of the #Force.",
        "location": "Luke's House, Tatooine",
        "links": {
          "self": "https://api.unsplash.com/users/lukeskywalker",
          "html": "https://unsplash.com/@lukeskywalker",
          "photos": "https://api.unsplash.com/users/lukeskywalker/photos",
          "likes": "https://api.unsplash.com/users/lukeskywalker/likes",
          "portfolio": "https://api.unsplash.com/users/lukeskywalker/portfolio",
          "following": "https://api.unsplash.com/users/lukeskywalker/following",
          "followers": "https://api.unsplash.com/users/lukeskywalker/followers"
        },
        "profile_image": {
          "small": "https://images.unsplash.com/profile-1446404465118-3a53b909cc82?ixlib=rb-0.3.5&q=80&fm=jpg&crop=faces&cs=tinysrgb&fit=crop&h=32&w=32&s=a2f8c40e39b8dfee1534eb32acfa6bc7",
          "medium": "https://images.unsplash.com/profile-1446404465118-3a53b909cc82?ixlib=rb-0.3.5&q=80&fm=jpg&crop=faces&cs=tinysrgb&fit=crop&h=64&w=64&s=3ef46b07bb19f68322d027cb8f9ac99f",
          "large": "https://images.unsplash.com/profile-1446404465118-3a53b909cc82?ixlib=rb-0.3.5&q=80&fm=jpg&crop=faces&cs=tinysrgb&fit=crop&h=128&w=128&s=27a346c2362207494baa7b76f5d606e5"
        },
        "total_collections": 6,
        "instagram_username": "hamillhimself",
        "total_likes": 1805,
        "total_photos": 119
      },
      "urls": {
        "raw": "https://images.unsplash.com/5/unsplash-kitsune-4.jpg?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=bc01c83c3da0425e9baa6c7a9204af81",
        "full": "https://images.unsplash.com/5/unsplash-kitsune-4.jpg?ixlib=rb-0.3.5&q=85&fm=jpg&crop=entropy&cs=srgb&ixid=eyJhcHBfaWQiOjEyMDd9&s=ce40ce8b8ba365e5e6d06401e5485390",
        "regular": "https://images.unsplash.com/5/unsplash-kitsune-4.jpg?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjEyMDd9&s=fb86e2e09fceac9b363af536b93a1275",
        "small": "https://images.unsplash.com/5/unsplash-kitsune-4.jpg?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max&ixid=eyJhcHBfaWQiOjEyMDd9&s=dd060fe209b4a56733a1dcc9b5aea53a",
        "thumb": "https://images.unsplash.com/5/unsplash-kitsune-4.jpg?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjEyMDd9&s=50827fd8476bfdffe6e04bc9ae0b8c02"
      },
      "links": {
        "self": "https://api.unsplash.com/photos/KR2mdHJ5qMg",
        "html": "https://unsplash.com/photos/KR2mdHJ5qMg",
        "download": "https://unsplash.com/photos/KR2mdHJ5qMg/download",
        "download_location": "https://api.unsplash.com/photos/KR2mdHJ5qMg/download"
      },
      "location": {
        "title": "Tatooine",
        "name": "Tatooine",
        "city": "Tatooine",
        "country": "Outer Rim Territories",
        "position": {
          "latitude": 45.5017,
          "longitude": 73.5673
        }
      },
      "exif": {
        "make": "Panasonic",
        "model": "DC-GH5",
        "exposure_time": "1/4000",
        "aperture": "7.1",
        "focal_length": "66",
        "iso": 1000
      },
      "views": 1383736,
      "downloads": 6344
    */

    @SerializedName("id")
    var id: String = ""

    @SerializedName("description")
    var description: String = ""

    @SerializedName("urls")
    var photoUrl: PhotoUrl = PhotoUrl()

    @SerializedName("user")
    var user: User = User()

    var width: Int = 0
    var height: Int = 0

    var created_at: String? = null
    var updated_at: String? = null

    var color: String? = null

    @SerializedName("downloads")
    var downloads: Int = 0

    var likes: Int = 0
    var liked_by_user: Boolean = false

    var location: Location? = null

    var current_user_collections: List<Collection>? = null

    var links: PhotoLinks? = null

    var exif: Exif ?= null

    lateinit var story: Story


    override val layoutRes: Int
        get() = R.layout.fragment_photos

    override val type: Int
        get() = R.id.item_photo_photo

    override fun getViewHolder(v: View): ViewHolder {
        return ViewHolder(LayoutInflater.from(v.context).inflate(R.layout.item_photo, null, false))
    }

    inner class ViewHolder(itemView: View) : FastAdapter.ViewHolder<Photo>(itemView) {

        var photo: SquareImage = itemView.findViewById(R.id.item_photo_photo)

        override fun bindView(item: Photo, payloads: MutableList<Any>) {

            Glide.with(itemView.context)
                .load(item.photoUrl.regular)
                .priority(Priority.HIGH)
                .transition(DrawableTransitionOptions.withCrossFade())
                .override(width, height)
                .placeholder(ColorDrawable(Color.parseColor(color)))
                .into(photo)
        }

        override fun unbindView(item: Photo) {

        }
    }

}