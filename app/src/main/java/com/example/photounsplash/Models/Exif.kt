package com.example.photounsplash.Models

class Exif {

      /*
         "make": "Canon",
         "model": "Canon EOS 40D",
         "exposure_time": "0.011111111111111112",
         "aperture": "4.970854",
         "focal_length": "37",
         "iso": 100
     */

    var make: String? = null
    var model: String? = null
    var exposure_time: String? = null
    var aperture: String? = null
    var focal_length: String? = null
    var iso: Int = 0

}