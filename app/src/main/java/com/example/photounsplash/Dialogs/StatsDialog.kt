package com.example.photounsplash.Dialogs

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import androidx.fragment.app.DialogFragment
import com.example.photounsplash.Models.Photo
import android.app.AlertDialog
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import butterknife.BindView
import com.example.photounsplash.R
import android.widget.ProgressBar
import android.widget.LinearLayout
import com.example.photounsplash.Models.PhotoStats
import com.example.photounsplash.Utils.Constants
import com.example.photounsplash.Webservices.ApiInterface
import com.example.photounsplash.Webservices.ServiceGenerator
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class StatsDialog : DialogFragment() {

    lateinit var statsContainer: LinearLayout
    lateinit var tvLikes: TextView
    lateinit var tvViews: TextView
    lateinit var tvDownloads: TextView
    lateinit var progressBar: ProgressBar

    var photo: Photo = Photo()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        super.onCreateDialog(savedInstanceState)

        val view = LayoutInflater.from(activity).inflate(R.layout.dialog_stats, null, false)

        initField(view)
        initData()

        return AlertDialog.Builder(getActivity())
            .setView(view)
            .create()
    }

    private fun initField(view: View) {
        statsContainer = view.findViewById(R.id.stats_container)
        tvLikes = view.findViewById(R.id.tvStatsLikes)
        tvViews = view.findViewById(R.id.tvStatsViews)
        tvDownloads = view.findViewById(R.id.tvStatsDownloads)
        progressBar = view.findViewById(R.id.stats_progress)
    }

    private fun initData() {
        val serviceGenerator = ServiceGenerator()
        val apiInterface: ApiInterface = serviceGenerator.createSercive(ApiInterface::class.java)
        val call: Call<PhotoStats> = apiInterface.getPhotoStats(photo.id, Constants.DEFAULT_STATISTICS_RESOLUTION, Constants.DEFAULT_STATISTICS_QUANTITY)

        call.enqueue(object: Callback<PhotoStats> {
            override fun onFailure(call: Call<PhotoStats>, t: Throwable) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onResponse(call: Call<PhotoStats>, response: Response<PhotoStats>) {
                if (isAdded()) {
                    if (response.isSuccessful() && response.body() != null) {
                        tvLikes.text = response.body()!!.likes!!.total.toString()
                        tvViews.text = response.body()!!.views!!.total.toString()
                        tvDownloads.text = response.body()!!.downloads!!.total.toString()
                        progressBar.setVisibility(View.GONE);
                        statsContainer.setVisibility(View.VISIBLE);
                    }
                }
            }
        })
    }


}