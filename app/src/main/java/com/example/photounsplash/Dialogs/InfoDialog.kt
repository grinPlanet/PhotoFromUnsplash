package com.example.photounsplash.Dialogs

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import androidx.fragment.app.DialogFragment
import com.example.photounsplash.Models.Photo
import android.app.AlertDialog
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import butterknife.BindView
import com.example.photounsplash.R

class InfoDialog : DialogFragment() {

    private lateinit var tvInfoDimensions: TextView
    private lateinit var tvInfoMake: TextView
    private lateinit var tvInfoModel: TextView
    private lateinit var tvInfoExposure: TextView
    private lateinit var tvInfoAperture: TextView
    private lateinit var tvInfoIso: TextView
    private lateinit var tvInfoFocalLength: TextView

    var photo: Photo = Photo()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        super.onCreateDialog(savedInstanceState)

        val view = LayoutInflater.from(activity).inflate(R.layout.dialog_info, null, false)

        initField(view)
        initData()

        return AlertDialog.Builder(getActivity())
            .setView(view)
            .create()
    }

    private fun initField(view: View) {
        tvInfoDimensions = view.findViewById(R.id.tvInfoDimensions)
        tvInfoMake = view.findViewById(R.id.tvInfoMake)
        tvInfoModel = view.findViewById(R.id.tvInfoModel)
        tvInfoExposure = view.findViewById(R.id.tvInfoExposure)
        tvInfoAperture = view.findViewById(R.id.tvInfoAperture)
        tvInfoIso = view.findViewById(R.id.tvInfoIso)
        tvInfoFocalLength = view.findViewById(R.id.tvInfoFocalLength)
    }

    private fun initData() {
        if (photo != null) {
            tvInfoDimensions.setText(if (photo.width === 0 || photo.height === 0) "-----" else "Dimensions " + ": " + photo.width + " x " + photo.height)
            tvInfoMake.setText(if (photo.exif?.make == null) "-----" else "Camera " + ": " + photo.exif!!.make)
            tvInfoModel.setText(if (photo.exif?.model == null) "-----" else "Camera model" + ": " + photo.exif!!.model)
            tvInfoExposure.setText(if (photo.exif?.exposure_time == null) "-----" else "Exposure time " + ": " + photo.exif!!.exposure_time)
            tvInfoAperture.setText(if (photo.exif?.aperture == null) "-----" else "Aperture " + ": " + photo.exif!!.aperture)
            tvInfoIso.setText(
                if (photo.exif?.iso === 0) "-----" else "ISO " + ": " + photo.exif?.iso
            )
            tvInfoFocalLength.setText(if (photo.exif?.focal_length == null) "-----" else "Focal length" + ": " + photo.exif!!.focal_length)
        }
    }


}