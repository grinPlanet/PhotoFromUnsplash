package com.example.photounsplash.Utils

import android.content.Context
import android.util.DisplayMetrics
import android.view.WindowManager
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.example.photounsplash.R
import kotlinx.android.synthetic.main.content_main.view.*

class Functions {
    companion object {
        fun changeMainFragment(fragmentActivity: FragmentActivity, fragment: Fragment) {
            fragmentActivity.supportFragmentManager
                .beginTransaction()
                .replace(R.id.main_conteiner, fragment)
                .commit()
        }
        fun getScreenHeightInDPs(context: Context): Int {
            val dm = DisplayMetrics()

            val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            windowManager.defaultDisplay.getMetrics(dm)

            return Math.round(dm.heightPixels / dm.density)
        }
    }
}