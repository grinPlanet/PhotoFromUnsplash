package com.example.photounsplash.Utils

import android.content.Context
import android.util.DisplayMetrics
import android.view.WindowManager

class Constants {
    companion object {
        val BASE_APP_URL: String = "https://api.unsplash.com/"
        val APPLICATION_ID: String = "3c0ade0513dc87b9c13074b5daf2076a314b93c3119f0b7b0a6aec7315d49610"
        val DEFAULT_STATISTICS_QUANTITY = 30
        val DEFAULT_STATISTICS_RESOLUTION = "days"
    }
}