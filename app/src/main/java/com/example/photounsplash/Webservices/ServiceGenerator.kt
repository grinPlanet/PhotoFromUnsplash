package com.example.photounsplash.Webservices

import com.example.photounsplash.Utils.Constants
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException

class ServiceGenerator {
    private var retrofit: Retrofit ?= null
    private var gson: Gson = GsonBuilder().create()

    private var httpLoggingInterceptor: HttpLoggingInterceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

    private var okHttpClientBuilder: OkHttpClient.Builder = OkHttpClient.Builder()
        .addInterceptor(httpLoggingInterceptor)
        .addInterceptor(object: Interceptor{
            override fun intercept(chain: Interceptor.Chain): Response {
                var request: Request = chain.request().newBuilder()
                    .addHeader("Authorization", "Client-ID " + Constants.APPLICATION_ID)
                    .build()
                return chain.proceed(request)
            }
        })
    private var okHttpClient: OkHttpClient = okHttpClientBuilder.build()

    fun <T> createSercive(serciveClass: Class<T>): T {
        if(retrofit == null) {
            retrofit = Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(Constants.BASE_APP_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
        }
        return retrofit!!.create(serciveClass)
    }

}