package com.example.photounsplash.Webservices

import com.example.photounsplash.Models.Collection
import com.example.photounsplash.Models.Photo
import com.example.photounsplash.Models.PhotoStats
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiInterface {
    @GET("photos")
    fun getPhotos(): Call<MutableList<Photo>>

    @GET("photos")
    fun getPhotos(
        @Query("page") page: Int?,
        @Query("per_page") per_page: Int?,
        @Query("order_by") order_by: String
    ): Call<MutableList<Photo>>

    @GET("photos/{id}/statistics")
    fun getPhotoStats(
        @Path("id") id: String,
        @Query("resolution") resolution: String,
        @Query("quantity") quantity: Int?
    ): Call<PhotoStats>

    @GET("photos/{id}")
    fun getAPhoto(@Path("id") id: String): Call<Photo>

    @GET("collections/featured")
    fun getCollections(): Call<List<Collection>>

    @GET("collections/{id}")
    fun getInformationOfCollection(@Path("id") id: Int): Call<Collection>

    @GET("collections/{id}/photos")
    fun getPhotosOfCollection(@Path("id") id: Int): Call<List<Photo>>
}