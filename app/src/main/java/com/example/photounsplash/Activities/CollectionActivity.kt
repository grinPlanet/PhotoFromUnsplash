package com.example.photounsplash.Activities

import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.example.photounsplash.Adapters.PhotosAdapter
import com.example.photounsplash.Fragments.CollectionsFragment
import com.example.photounsplash.Models.Collection
import com.example.photounsplash.Models.Photo
import com.example.photounsplash.R
import com.example.photounsplash.Webservices.ApiInterface
import com.example.photounsplash.Webservices.ServiceGenerator
import com.google.gson.Gson
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CollectionActivity :  AppCompatActivity() {
    private val TAG: String = CollectionActivity::class.java.simpleName

    lateinit var progressBar: ProgressBar
    lateinit var recyclerView: RecyclerView

    lateinit var userName: TextView
    lateinit var description: TextView
    lateinit var title: TextView

    lateinit var userAvatar: CircleImageView

    private var photos: MutableList<Photo> = ArrayList()
    lateinit private var photosAdapter: PhotosAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_collection)

        val toolbar: Toolbar = findViewById(R.id.activity_collection_detail_toolbar)

        val upArrow = resources.getDrawable(R.drawable.abc_ic_ab_back_material, theme)
        upArrow.setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_ATOP)
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeAsUpIndicator(upArrow)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        progressBar = findViewById(R.id.fragment_collection_pb)
        recyclerView = findViewById(R.id.fragment_collection_rv)

        userName = findViewById(R.id.fragment_collection_username)
        description = findViewById(R.id.fragment_collection_description)
        //title = findViewById(R.id.fragment_collection_title)
        userAvatar = findViewById(R.id.fragment_collection_user_avatar)

        val linearLayoutManager: LinearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager
        photosAdapter = PhotosAdapter(this, photos)
        recyclerView.adapter = photosAdapter

        val collectionId: Int = intent.getIntExtra(CollectionsFragment.COLLECTION_ID, 0)
        val collection: Collection = Gson().fromJson(intent.getStringExtra(CollectionsFragment.COLLECTION), Collection::class.java) as Collection

        if(collection.description.trim().length != 0) {
            description.text = collection.description
        } else {
            description.visibility = View.GONE
        }
        userName.text = collection.user.username
        Glide.with(this@CollectionActivity)
            .load(collection.user.profileImage.medium)
            .priority(Priority.HIGH)
            .into(userAvatar)



        findViewById<TextView>(R.id.activity_collection_detail_toolbar_title).text = collection.title

        showProgressBar(true)
        getPhotosOfCollection(collectionId)
    }

    private fun showProgressBar(isShow: Boolean) {
        if(isShow) {
            progressBar.visibility = View.VISIBLE
            recyclerView.visibility = View.GONE
        } else {
            progressBar.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
        }
    }

    fun getPhotosOfCollection(collectionId: Int) {
        val serviceGenerator: ServiceGenerator = ServiceGenerator()
        val apiInterface: ApiInterface = serviceGenerator.createSercive(ApiInterface::class.java)
        val call: Call<List<Photo>> = apiInterface.getPhotosOfCollection(collectionId)

        call.enqueue(object: Callback<List<Photo>> {
            override fun onFailure(call: Call<List<Photo>>, t: Throwable) {
                Log.e(TAG, t.message)
                showProgressBar(false)
            }

            override fun onResponse(call: Call<List<Photo>>, response: Response<List<Photo>>) {
                if(response.isSuccessful) {
                    photos.addAll(response.body()!!)
                    photosAdapter.notifyDataSetChanged()
                } else {
                    Log.e(TAG, "fail ${response.message()}")
                }
                showProgressBar(false)
            }

        })
    }

}