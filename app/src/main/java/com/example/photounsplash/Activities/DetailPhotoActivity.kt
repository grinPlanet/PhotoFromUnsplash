package com.example.photounsplash.Activities

import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import butterknife.BindView
import butterknife.internal.Utils
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.request.RequestOptions
import com.example.photounsplash.Dialogs.InfoDialog
import com.example.photounsplash.Dialogs.StatsDialog
import com.example.photounsplash.Models.Photo
import com.example.photounsplash.R
import com.example.photounsplash.Utils.SquareImage
import com.example.photounsplash.Webservices.ApiInterface
import com.example.photounsplash.Webservices.ServiceGenerator
import com.github.clans.fab.FloatingActionButton
import com.github.clans.fab.FloatingActionMenu
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class DetailPhotoActivity : AppCompatActivity() {

    val TAG: String = DetailPhotoActivity::class.java.simpleName

    lateinit var photo: Photo
    lateinit var photoDetail: Photo

    lateinit var floatingActionMenu: FloatingActionMenu

    lateinit var colorIcon: Drawable

    lateinit var tvUser: TextView
    lateinit var tvTitle: TextView
    lateinit var tvDescription: TextView
    lateinit var tvLocation: TextView
    lateinit var tvDate: TextView
    lateinit var tvLikes: TextView
    lateinit var tvColor: TextView
    lateinit var tvDownloads: TextView
    lateinit var fabDownload: FloatingActionButton
    lateinit var fabWallpaper: FloatingActionButton
    lateinit var fabStats: FloatingActionButton
    lateinit var fabInfo: FloatingActionButton
    lateinit var detailProgress: ProgressBar
    lateinit var linearLayoutInfo: LinearLayout
    lateinit var imgFull: SquareImage

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_photo)

        colorIcon = getResources().getDrawable(R.drawable.ic_fiber_manual_record_black_18dp, getTheme());

        init()
        floatingActionMenu = findViewById(R.id.fab_menu)
        floatingActionMenu.setClosedOnTouchOutside(true)

        fabInfo.setOnClickListener(onClickListener)
        fabStats.setOnClickListener(onClickListener)

        toolBar()

        photo = Gson().fromJson(intent.getStringExtra("Photo"), Photo::class.java) as Photo

        Glide.with(this)
            .load(photo.photoUrl.regular)
            .apply(RequestOptions().priority(Priority.HIGH))
            .into(findViewById(R.id.activity_detail_photo_square_image))

        photoDetail()

    }

    fun init() {
        tvTitle = findViewById(R.id.tvTitle)
        tvDescription = findViewById(R.id.tvDescription)
        tvLocation = findViewById(R.id.tvLocation)
        tvDate = findViewById(R.id.tvDate)
        tvLikes = findViewById(R.id.tvLikes)
        tvColor = findViewById(R.id.tvColor)
        tvDownloads = findViewById(R.id.tvDownloads)
//        fabDownload = findViewById(R.id.fabDownload)
//        fabWallpaper = findViewById(R.id.fabWallpaper)
        fabStats = findViewById(R.id.fab_stats)
        fabInfo = findViewById(R.id.fab_info)
        detailProgress = findViewById(R.id.detail_progress)
        linearLayoutInfo = findViewById(R.id.activity_detail_photo_ll_info)
        linearLayoutInfo.visibility = View.GONE

        imgFull = findViewById(R.id.activity_detail_photo_square_image)

    }

    fun toolBar() {
        val toolbar: Toolbar = findViewById(R.id.toolbar_detail)
        val upArrow = resources.getDrawable(R.drawable.abc_ic_ab_back_material, theme)
        upArrow.setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_ATOP)
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeAsUpIndicator(upArrow)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setTitle("")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    fun loadShortInfo() {
        Glide.with(this)
            .load(photoDetail.user.profileImage.medium)
            .into(findViewById(R.id.activity_detail_photo_user_avatar))
        findViewById<TextView>(R.id.activity_detail_photo_username).text = "${photoDetail.user.name}"

        if (photoDetail.story != null && photoDetail.story.title != null) {
            tvTitle.text = photoDetail.story!!.title
        } else {
            tvTitle.setVisibility(View.GONE)
        }

        if (photoDetail.description != null && !photoDetail.description.isEmpty()) {
            tvDescription.text = photoDetail.description
        } else {
            tvDescription.setVisibility(View.GONE)
        }

        if (photoDetail.location != null) {
            if (photoDetail.location!!.title != null) {
                tvLocation.text = photoDetail.location!!.title
            } else if (photoDetail.location!!.city != null && photoDetail.location!!.country != null) {
                tvLocation.setText(photoDetail.location!!.city + ", " + photoDetail.location!!.country)
            }else if(photoDetail.location!!.city != null) {
                tvLocation.setText(photoDetail.location!!.city)
            }else if(photoDetail.location!!.country != null) {
                tvLocation.setText(photoDetail.location!!.country)
            }
        } else {
            tvLocation.setVisibility(View.GONE)
        }

        tvDate.setText(photoDetail.created_at!!.split("T")[0])
        tvLikes.text = "${photoDetail.likes} Likes"

        if (photoDetail.color != null) {
            colorIcon.setColorFilter(Color.parseColor(photoDetail.color), PorterDuff.Mode.SRC_IN)
            tvColor.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(this, R.drawable.ic_palette) , null, colorIcon, null )
        }

        tvColor.setText(photoDetail.color)

        tvDownloads.text = "${photoDetail.downloads} Downloads"
        detailProgress.visibility = View.GONE
        linearLayoutInfo.visibility = View.VISIBLE
        floatingActionMenu.visibility = View.VISIBLE

        imgFull.setOnClickListener(object: View.OnClickListener{
            override fun onClick(p0: View?) {
                val i: Intent = Intent(this@DetailPhotoActivity, PreviewActivity::class.java)
                i.putExtra("Photo", photoDetail.photoUrl.regular)
                startActivity(i)
            }
        })
    }

    fun photoDetail() {
        val serviceGenerator = ServiceGenerator()
        val apiInterface: ApiInterface = serviceGenerator.createSercive(ApiInterface::class.java)
        val call: Call<Photo> = apiInterface.getAPhoto(photo.id)

        call.enqueue(object : Callback<Photo> {
            override fun onFailure(call: Call<Photo>, t: Throwable) { }

            override fun onResponse(call: Call<Photo>, response: Response<Photo>) {
                if(response.isSuccessful) {
                    photoDetail = response.body()!!
                    loadShortInfo()
                }
            }
        })
    }

    val onClickListener = object: View.OnClickListener{
        override fun onClick(p0: View?) {
            when(p0!!.id) {
                R.id.fab_info -> {
                    floatingActionMenu.close(true)
                    val infoDialog: InfoDialog = InfoDialog()
                    infoDialog.photo = photoDetail
                    infoDialog.show(supportFragmentManager, null)
                }
                R.id.fab_stats -> {
                    floatingActionMenu.close(true)
                    val infoDialog: StatsDialog = StatsDialog()
                    infoDialog.photo = photoDetail
                    infoDialog.show(supportFragmentManager, null)
                }
            }
        }

    }


}