package com.example.photounsplash.Fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout.VERTICAL
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import butterknife.*
import com.example.photounsplash.Activities.DetailPhotoActivity
import com.example.photounsplash.Adapters.PhotosAdapter
import com.example.photounsplash.Models.Photo
import com.example.photounsplash.R
import com.example.photounsplash.Utils.Constants
import com.example.photounsplash.Webservices.ApiInterface
import com.example.photounsplash.Webservices.ServiceGenerator
import com.google.gson.Gson
import com.kc.unsplash.Unsplash
import com.kc.unsplash.api.Order
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.scroll.EndlessRecyclerOnScrollListener
import com.mikepenz.fastadapter.ui.items.ProgressItem
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PhotosFragment : Fragment() {

    private val TAG: String = PhotosFragment::class.java.simpleName

    lateinit var progressBar: ProgressBar
    lateinit var recyclerView: RecyclerView
    lateinit var swipeRefreshLayout: SwipeRefreshLayout

    var page = 1

    lateinit var itemAdapter: ItemAdapter<Photo>
    lateinit var fastAdapter: FastAdapter<Photo>

    var isNeeedToLoadMore = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_photos, container, false)
        progressBar = view.findViewById(R.id.fragment_photos_pb)
        swipeRefreshLayout = view.findViewById(R.id.fragment_photos_swipe_refresh)
        itemAdapter = ItemAdapter()
        fastAdapter = FastAdapter.with(itemAdapter)

        val layoutManager = LinearLayoutManager(activity)
        recyclerView = view.findViewById(R.id.fragment_photos_rv)
        recyclerView.layoutManager = layoutManager

        recyclerView.setItemViewCacheSize(10)
        recyclerView.adapter = fastAdapter

        fastAdapter.onClickListener = { view, adapter, item, position ->

            val i = Intent(context, DetailPhotoActivity::class.java)
            i.putExtra("Photo", Gson().toJson(item))
            startActivity(i)

            false
        }

        recyclerView.addOnScrollListener(object: EndlessRecyclerOnScrollListener(layoutManager) {
            override fun onLoadMore(currentPage: Int) { }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (!recyclerView.canScrollVertically(-1)) {
                    onScrolledToTop()
                } else if (!recyclerView.canScrollVertically(1)) {
                    onScrolledToBottom()
                } else if (dy < 0) {
                    onScrolledUp()
                } else if (dy > 0) {
                    onScrolledDown()
                }
            }

            fun onScrolledUp() {}

            fun onScrolledDown() {
                if(isNeeedToLoadMore) {
                    val totalItemCount = recyclerView.layoutManager!!.itemCount
                    if(totalItemCount - 3 == layoutManager.findLastVisibleItemPosition() - 2) {
                        page++
                        loadPhotos()
                        isNeeedToLoadMore = false
                    }
                }
            }

            fun onScrolledToTop() {  }

            fun onScrolledToBottom() {  }
        })

        swipeRefreshLayout.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
            override fun onRefresh() {
                page = 1
                itemAdapter.clear()
                loadPhotos()
                Toast.makeText(getContext(), "Update", Toast.LENGTH_SHORT).show()
                swipeRefreshLayout.isRefreshing = false
                isNeeedToLoadMore = true
            }
        })

        showProgressBar(true)

        loadPhotos()

        return view
    }


    private fun loadPhotos() {
        val serviceGenerator = ServiceGenerator()
        val apiInterface: ApiInterface = serviceGenerator.createSercive(ApiInterface::class.java)
        val call: Call<MutableList<Photo>> = apiInterface.getPhotos(page, 30, "latest")

        call.enqueue(object : Callback<MutableList<Photo>> {
            override fun onFailure(call: Call<MutableList<Photo>>, t: Throwable) {
                showProgressBar(false)
            }

            override fun onResponse(call: Call<MutableList<Photo>>, response: Response<MutableList<Photo>>) {
                if (isAdded) {
                    if (response.isSuccessful) {
                        isNeeedToLoadMore = true
                        itemAdapter.add(response.body()!!)
                        showProgressBar(false)
                        //Toast.makeText(context, "$page", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        })
    }

    private fun getPhotos() {
        val serviceGenerator = ServiceGenerator()
        val apiInterface: ApiInterface = serviceGenerator.createSercive(ApiInterface::class.java)
        val call: Call<MutableList<Photo>> = apiInterface.getPhotos()

        call.enqueue(object : Callback<MutableList<Photo>> {
            override fun onFailure(call: Call<MutableList<Photo>>, t: Throwable) {
                showProgressBar(false)
            }

            override fun onResponse(call: Call<MutableList<Photo>>, response: Response<MutableList<Photo>>) {
                if (isAdded) {
                    if (response.isSuccessful) {
                        itemAdapter.add(response.body()!!)
                        showProgressBar(false)
                        isNeeedToLoadMore = true
                        //Toast.makeText(context, "$page", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        })
    }

//    fun initUnsp() {
//        val unsplash: Unsplash = Unsplash(Constants.APPLICATION_ID)
//
//        unsplash.getPhotos(1, 10, Order.LATEST, object : Unsplash.OnPhotosLoadedListener {
//            override fun onComplete(photos: MutableList<com.kc.unsplash.models.Photo>?) {
//                //itemAdapter.add(photos)
//                val photosCustom: MutableList<Photo> = mutableListOf()
//                //photos!!.forEach { photo -> Toast.makeText(context, "$photo", Toast.LENGTH_SHORT).show() }
//                photos!!.forEach { photo ->
//                    photosCustom.add(photo as Photo)
//
//                }
//
//            }
//
//
//            override fun onError(error: String) {
//                Log.v("Error", error)
//            }
//        })
//
//    }

    private fun showProgressBar(isShow: Boolean) {
        if (isShow) {
            progressBar.visibility = View.VISIBLE
            recyclerView.visibility = View.GONE
        } else {
            progressBar.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }
}