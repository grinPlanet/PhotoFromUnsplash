package com.example.photounsplash.Fragments

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.OnItemClick
import com.example.photounsplash.Activities.CollectionActivity
import com.example.photounsplash.Adapters.CollectionsAdapter
import com.example.photounsplash.Models.Collection
import com.example.photounsplash.Models.Photo
import com.example.photounsplash.R
import com.example.photounsplash.Webservices.ApiInterface
import com.example.photounsplash.Webservices.ServiceGenerator
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CollectionsFragment : Fragment(), CollectionsAdapter.OnItemClickListener {
    override fun onItemClick(position: Int) {
        val collection: Collection = collections[position]
        val intent = Intent(thisContext, CollectionActivity::class.java)
        intent.putExtra(COLLECTION_ID, collection.id)
        intent.putExtra(COLLECTION,  Gson().toJson(collection))
        startActivity(intent)
    }


    companion object {
        val COLLECTION_ID: String = "collectionId"
        val COLLECTION: String = "collection"
    }

    val TAG: String = CollectionsFragment::class.java.simpleName

    lateinit var recyclerView: RecyclerView
    lateinit var progressBar: ProgressBar

    lateinit var collectionsAdapter: CollectionsAdapter
    var collections: MutableList<Collection> = ArrayList()

    lateinit var thisContext: Context

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_collections, container, false)

        recyclerView = view.findViewById(R.id.fragment_collections_rv)
        progressBar = view.findViewById(R.id.fragment_collections_pb)

        val linearLayoutManager: LinearLayoutManager = LinearLayoutManager(activity)
        recyclerView.layoutManager = linearLayoutManager

        collectionsAdapter = CollectionsAdapter(view.context, collections)
        recyclerView.adapter = collectionsAdapter

        collectionsAdapter.setOnItemClickListener(this)

        showProgressBar(true)
        getCollections()

        thisContext = view.context

        return view
    }


    private fun getCollections() {
        val serviceGenerator: ServiceGenerator = ServiceGenerator()
        val apiInterface: ApiInterface = serviceGenerator.createSercive(ApiInterface::class.java)
        val call: Call<List<Collection>> = apiInterface.getCollections()

        call.enqueue(object: Callback<List<Collection>> {
            override fun onFailure(call: Call<List<Collection>>, t: Throwable) {
                Log.e(TAG, t.message)
                showProgressBar(false)
            }

            override fun onResponse(call: Call<List<Collection>>, response: Response<List<Collection>>) {
                if(response.isSuccessful) {
                    collections.addAll(response.body()!!)
                    collectionsAdapter.notifyDataSetChanged()

                } else {
                    Log.e(TAG, "fail ${response.message()}")
                }
                showProgressBar(false)
            }


        })

    }

    private fun showProgressBar(isShow: Boolean) {
        if(isShow) {
            progressBar.visibility = View.VISIBLE
            recyclerView.visibility = View.GONE
        } else {
            progressBar.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }
}