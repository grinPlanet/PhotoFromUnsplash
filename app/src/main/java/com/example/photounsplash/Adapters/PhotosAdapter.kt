package com.example.photounsplash.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.example.photounsplash.Models.Photo
import com.example.photounsplash.R
import com.example.photounsplash.Utils.SquareImage
import de.hdodenhof.circleimageview.CircleImageView

class PhotosAdapter : RecyclerView.Adapter<PhotosAdapter.ViewHolder> {

    private var context: Context
    private var photos: MutableList<Photo>


    constructor(context: Context, photos: MutableList<Photo>) {
        this.context = context
        this.photos = photos
    }

    fun addPhotos(photos: List<Photo>) {
        val lastCount = itemCount
        this.photos.addAll(photos)
        notifyItemRangeInserted(lastCount, photos.size)
    }

    fun clear() {
        val lastCount = this.photos.size
        this.photos.clear()
        notifyItemRangeInserted(0, lastCount)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView: View = LayoutInflater.from(parent.context).inflate(R.layout.item_photo, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return photos.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val photo: Photo = photos.get(position)

        Glide.with(context)
            .load(photo.photoUrl.regular)
            .transition(withCrossFade())
            .into(holder.photo)
    }

    class ViewHolder : RecyclerView.ViewHolder {
        @BindView(R.id.item_photo_photo) lateinit var photo: SquareImage
        constructor(itemView: View) : super(itemView) {
            photo = itemView.findViewById(R.id.item_photo_photo)
        }
    }
}