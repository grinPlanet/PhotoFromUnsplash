package com.example.photounsplash.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.example.photounsplash.Models.Collection
import com.example.photounsplash.R
import com.example.photounsplash.Utils.Constants
import com.example.photounsplash.Utils.Functions
import com.example.photounsplash.Utils.SquareImage

class CollectionsAdapter :  RecyclerView.Adapter<CollectionsAdapter.ViewHolder> {

    private var context: Context
    private var collections: MutableList<Collection>

    private lateinit var mListener: OnItemClickListener

    constructor(context: Context, collections: MutableList<Collection>) {
        this.context = context
        this.collections = collections
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView: View = LayoutInflater.from(parent.context).inflate(R.layout.item_collection, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return collections.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val collection: Collection = collections.get(position)

        holder.title.text = collection.title
        holder.totalPhotos.text = collection.totalPhotos.toString() + " phtos"

        Glide.with(context)
            .load(collection.coverPhoto.photoUrl.regular)
            //.override(600)
            .override((Functions.getScreenHeightInDPs(context) / 1.2f).toInt())
            .priority(Priority.HIGH)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(holder.collectionPhoto)
    }

    inner class ViewHolder : RecyclerView.ViewHolder, View.OnClickListener {
        var title: TextView
        var totalPhotos: TextView
        var collectionPhoto: SquareImage

        constructor(itemView: View) : super(itemView) {
            title = itemView.findViewById(R.id.item_collection_title)
            totalPhotos = itemView.findViewById(R.id.item_collection_total_photo)
            collectionPhoto = itemView.findViewById(R.id.item_collection_photo)

            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            if (mListener != null) {
                val position = adapterPosition
                if (position != androidx.recyclerview.widget.RecyclerView.NO_POSITION) {
                    mListener.onItemClick(position)
                }
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        mListener = listener
    }

}